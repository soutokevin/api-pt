import Router from '@koa/router';
import Post from '../models/posts';

const posts = new Router({ prefix: '/posts' });

posts.get('/', async ctx => {
  ctx.status = 200;
  ctx.body = await Post.all();
});

posts.post('/', async ctx => {
  ctx.status = 200;
  ctx.body = await Post.create(ctx.request.body);
});

posts.get('/:id', async ctx => {
  const book = await Post.find(ctx.params.id);

  if (book) {
    ctx.status = 200;
    ctx.body = book;
  }
});

posts.patch('/:id', async ctx => {
  const book = await Post.update(ctx.params.id);

  if (book) {
    ctx.status = 200;
    ctx.body = book;
  }
});

posts.delete('/:id', async ctx => {
  const book = await Post.delete(ctx.params.id);

  if (book) {
    ctx.status = 200;
    ctx.body = book;
  }
});

export default posts;
