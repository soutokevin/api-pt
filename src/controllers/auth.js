import Router from '@koa/router';
import jwt from 'jsonwebtoken';
import Users from '../models/users';

const auth = new Router();

auth.post('/signin', async ctx => {
  try {
    const user = await Users.authenticate(ctx.request.body);

    if (user) {
      ctx.status = 200;
      ctx.body = { token: jwt.sign(user, 'super-secret') };
    }
  } catch {
    ctx.status = 401
  }
});

auth.post('/signup', async ctx => {
  ctx.status = 200;
  ctx.body = await Users.create(ctx.request.body);
});

export default auth;
