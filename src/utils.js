const clearSeq =
  process.platform === 'win32' ? '\x1B[2J\x1B[0f' : '\x1B[2J\x1B[3J\x1B[H';

export function clearConsole() {
  process.stdout.write(clearSeq);
}

export function startupMessage()  {
  const { address, port } = this.address();
  clearConsole();

  if (address === '0.0.0.0') {
    console.log(`Server listening on port ${port}`);
  } else {
    console.log(`Server listening on ${address}:${port}`);
  }
}
