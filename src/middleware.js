import parse from 'co-body';

export const json = async (ctx, next) => {
  if (ctx.request.is('json')) {
    ctx.request.body = await parse.json(ctx);
  }

  return next();
};
