import nanoid from 'nanoid';

const fakeDB = [];

export default {
  async all() {
    return fakeDB;
  },
  async create({ creator, content }) {
    const id = nanoid(10);
    const post = { id, creator, content };
    fakeDB.push(post);
    return post;
  },
  async find(id) {
    return fakeDB.find(post => post.id === id);
  },
  async fromUser(id) {
    return fakeDB.filter(post => post.creator === id);
  },
  async update({ id, content }) {
    const post = await this.find(id);

    if (post) {
      Object.assign(post, { content });
      return post;
    } else {
      return null;
    }
  },
  async delete(id) {
    const idx = fakeDB.findIndex(post => post.id === id);

    if (idx < 0) {
      return null;
    } else {
      const post = fakeDB[idx];
      fakeDB.splice(idx, 1);
      return post;
    }
  },
};
