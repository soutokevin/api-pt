import nanoid from 'nanoid';

const fakeDB = [];

export default {
  async all() {
    return fakeDB;
  },
  async create({ name, email, password }) {
    const id = nanoid(10);
    const user = { id, name, email, password };
    fakeDB.push(user);
    return user;
  },
  async find(id) {
    return fakeDB.find(user => user.id === id);
  },
  async authenticate({ email, password }) {
    const user = fakeDB.find(user => user.email === email);

    if (user.password === password) {
      return user
    } else {
      throw new Error('Invalid password')
    }
  },
  async update({ id, name, email, password }) {
    const post = await this.find(id);

    if (post) {
      if (name) post.name = name;
      if (email) post.email = email;
      if (password) post.password = password;
      return post;
    } else {
      return null;
    }
  },
  async delete(id) {
    const idx = fakeDB.findIndex(post => post.id === id);

    if (idx < 0) {
      return null;
    } else {
      const post = fakeDB[idx];
      fakeDB.splice(idx, 1);
      return post;
    }
  },
};
