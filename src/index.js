import Koa from 'koa';
import { createServer } from 'http';
import { startupMessage } from './utils';

const PORT = process.env.PORT || 8080;
const HOST = process.env.HOST || '0.0.0.0';
const srv = new Koa();

import logger from 'koa-logger';
srv.use(logger());

import cors from '@koa/cors';
srv.use(cors());

import { json } from './middleware';
srv.use(json);

import auth from './controllers/auth';
srv.use(auth.middleware());

import jwt from 'koa-jwt';
srv.use(jwt({ secret: 'super-secret' }));

import posts from './controllers/posts';
srv.use(posts.middleware());

const server = createServer(srv.callback());
server.listen(PORT, HOST, startupMessage);
